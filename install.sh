#!/bin/bash

if [ "$(id -u)" != "0" ]; then
    echo "You need to sudo this script"
    echo ""
    echo "sudo ./install.sh"
    echo ""
    
    exit 1
    
fi

# Get nal directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "nal.py dir=$DIR"

sed 's|<nal_path>|'$DIR'/|g' $DIR/nal.desktop | sed 's|<nal_icon_path_and_name>|'$DIR'/nal.png|g' > /usr/share/applications/nal.desktop
